FROM php:8.0-fpm-alpine
ARG APP_ENV
ARG APP_HTTP_PORT
ARG BASE_DIR
ARG DATABASE_HOST
ARG DATABASE_PORT
ARG PROJECT_USER_NAME=app
ARG PROJECT_USER_ID=1000
ARG PROJECT_GROUP_ID=1000
ARG YARN_CACHE_DIR

ENV APP_ENV=$APP_ENV \
    APP_HTTP_PORT=$APP_HTTP_PORT \
    BASE_DIR=$BASE_DIR \
    ENV="/home/${PROJECT_USER_NAME}/.ashrc" \
    BASH_ENV="/home/${PROJECT_USER_NAME}/.bashrc" \
    YARN_CACHE_DIR=$YARN_CACHE_DIR

RUN set -ex ; \
    \
    # Create app user
    existing_group=$(getent group "${PROJECT_GROUP_ID}" | cut -d: -f1) ; \
    if [[ -n "${existing_group}" ]] ; then delgroup "${existing_group}" ; fi ; \
    existing_user=$(getent passwd "${PROJECT_USER_ID}" | cut -d: -f1) ; \
    if [[ -n "${existing_user}" ]] ; then deluser "${existing_user}" ; fi ; \
    \
        addgroup -g "${PROJECT_GROUP_ID}" -S "${PROJECT_USER_NAME}" ; \
        adduser -u "${PROJECT_USER_ID}" -D -S -s /bin/bash -G "${PROJECT_USER_NAME}" "${PROJECT_USER_NAME}" ; \
        adduser "${PROJECT_USER_NAME}" www-data ; \
        sed -i "/^${PROJECT_USER_NAME}/s/!/*/" /etc/shadow ; \
    \
    # Build Dependencies
    apk add --no-cache --update --virtual buildDeps \
        autoconf \
        g++ \
        make ; \
    # Install packages
    apk add --no-cache --update \
        bash \
        curl \
        git \
        glib \
        icu-dev \
        libgcc \
        libstdc++ \
        libintl \
        libpng-dev \
        libx11 \
        libxext \
        libxrender \
        nano \
        nginx \
        nginx-mod-http-headers-more \
        npm \
        runit \
        screen \
        sudo \
        ttf-dejavu \
        ttf-droid \
        ttf-freefont \
        ttf-liberation  \
        zstd-dev ; \
    # Install PHP packages
    docker-php-ext-install intl pdo_mysql ; \
    yes | pecl install -o -f igbinary ; \
    docker-php-ext-enable intl pdo_mysql sodium ; \
    # Install yarn
    npm install --global yarn ; \
    \
    # Configure environment \
    touch /env $ENV $BASH_ENV && chown "${PROJECT_USER_NAME}" /env $ENV $BASH_ENV ; \
    install -o "${PROJECT_USER_NAME}" -g www-data -d ${BASE_DIR} ; \
    install -o nginx -g nginx -d /run/nginx ; \
    chown -R nginx:nginx /etc/nginx ; \
    echo ""${PROJECT_USER_NAME}" ALL=(root) NOPASSWD:SETENV:ALL" >> /etc/sudoers.d/"${PROJECT_USER_NAME}" ; \
    \
    # Clean up
    docker-php-source delete ; \
    apk del buildDeps ; \
    rm -rf /tmp/* /var/cache/* /etc/nginx/conf.d /etc/nginx/http.d /etc/nginx/sites-available ;

COPY --from=composer /usr/bin/composer /usr/local/bin/composer
COPY ./docker/app/config/php/php.${APP_ENV}.ini /usr/local/etc/php/php.ini
COPY ./docker/app/config/php/conf.d/ /usr/local/etc/php/conf.d/
COPY ./docker/app/config/php-fpm.d/ /usr/local/etc/php-fpm.d/
COPY ./docker/app/config/nginx/ /etc/nginx/
COPY ./docker/app/bin/ /usr/local/bin/
COPY ./docker/app/sbin/ /sbin/
COPY ./docker/app/etc/ /etc/
COPY --chown="${PROJECT_USER_NAME}":www-data . ${BASE_DIR}

USER $PROJECT_USER_NAME
WORKDIR $BASE_DIR

CMD set-env ; \
    composer-install ; \
    wait-for-it ${DATABASE_HOST}:${DATABASE_PORT} -- bin/console doctrine:migrations:migrate -n --no-debug ; \
    bin/console cache:warm ; \
    sudo /sbin/runit-wrapper

EXPOSE 8080 9000
