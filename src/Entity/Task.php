<?php

namespace App\Entity;

use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Annotations as OA;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use UnexpectedValueException;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @OA\Property(description="The unique identifier of the task.")
     *
     * @Groups({"task_response_body"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=10, unique=true)
     *
     * @OA\Property(description="The unique task name.")
     *
     * @Groups({"task_response_body", "task_request_body"})
     */
    private string $task;

    /**
     * @ORM\Column(type="string", length=500)
     *
     * @OA\Property(description="The task description.")
     *
     * @Groups({"task_response_body", "task_request_body"})
     */
    private string $description;

    /**
     * @ORM\Column(type="smallint")
     *
     * @OA\Property(description="The task status. 1=Pending 2='In Progress' 3=Done 4=Closed")
     *
     * @Groups({"task_response_body", "task_request_body"})
     */
    private int $status;

    /**
     * @ORM\Column(type="datetime")
     *
     * @OA\Property(description="The task creation timestamp in ATOM format.")
     *
     * @Groups({"task_response_body"})
     */
    private DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime")
     *
     * @OA\Property(description="The last task update timestamp in ATOM format.")
     *
     * @Groups({"task_response_body"})
     */
    private DateTimeInterface $updatedAt;

    public function __construct(string $task = '', string $description = '', int $status = 1)
    {
        $this->task = $task;
        $this->description = $description;
        $this->status = $status;
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }

    public static function fromFormFactory(FormInterface $form, ?Task $task = null): Task
    {
        /** @psalm-var array<string, string|int|null> $data */
        $data = $form->getData();
        $task ??= new self();
        $task
            ->setTask(strval($data['task']))
            ->setDescription(strval($data['description']))
            ->setStatus(intval($data['status']));

        return $task;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTask(): string
    {
        return $this->task;
    }

    public function setTask(string $task): self
    {
        $this->task = $task;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @psalm-return list<int>
     */
    public static function statuses(): array
    {
        return StatusConstants::ALL;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        if (!in_array($status, static::statuses())) {
            throw new UnexpectedValueException("Invalid status $status given.");
        }

        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @psalm-return array{id: int|null, task:string, description:string, status:int, createdAt:string, updatedAt:string}
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'task' => $this->task,
            'description' => $this->description,
            'status' => $this->status,
            'createdAt' => $this->createdAt->format(DateTimeInterface::ATOM),
            'updatedAt' => $this->updatedAt->format(DateTimeInterface::ATOM),
        ];
    }

    public function compareTo(Task $other): int
    {
        return (int) !(
            $this->task === $other->getTask()
            && $this->description === $other->getDescription()
            && $this->status === $other->getStatus()
        );
    }
}
