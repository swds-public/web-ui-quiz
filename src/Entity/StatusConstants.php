<?php

declare(strict_types=1);

namespace App\Entity;

class StatusConstants
{
    public const PENDING = 1;
    public const IN_PROGRESS = 2;
    public const DONE = 3;
    public const CLOSED = 4;

    public const ALL = [
        self::PENDING,
        self::IN_PROGRESS,
        self::DONE,
        self::CLOSED,
    ];

    public static function isValidStatus(int $status): bool
    {
        return in_array($status, self::ALL);
    }
}
