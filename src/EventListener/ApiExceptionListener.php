<?php

declare(strict_types=1);

namespace App\EventListener;

use App\HttpKernel\ApiException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiExceptionListener
{
    public function __construct(private LoggerInterface $logger)
    {
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $throwable = $event->getThrowable();

        if ($throwable instanceof ApiException) {
            $response = $this->toResponse($throwable->getMessage(), $throwable->getStatusCode());
        } elseif ($throwable instanceof MethodNotAllowedHttpException
            || $throwable instanceof NotFoundHttpException
        ) {
            $response = $this->toResponse($throwable->getMessage(), 400);
        } else {
            $response = $this->toResponse('Service error', 500);
            $this->logger->error($event->getThrowable());
        }
        $response->headers->set('Content-Type', 'application/problem+json');
        $event->setResponse($response);
    }

    private function toResponse(string $message, int $statusCode): JsonResponse
    {
        return new JsonResponse(
            [
                'message' => [
                    'text' => $message,
                    'level' => 'error',
                    'code' => $statusCode,
                ],
            ],
            $statusCode
        );
    }
}
