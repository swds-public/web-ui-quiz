<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\StatusConstants;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('task', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Task name cannot be blank']),
                    new Length([
                        'min' => 1,
                        'max' => 10,
                        'minMessage' => 'Enter at least 1 character',
                        'maxMessage' => 'You entered {{ value }} but you can not use more than {{ limit }} characters',
                    ]),
                ],
            ])
            ->add('description', TextareaType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'The description cannot be blank']),
                    new Length([
                        'min' => 1,
                        'max' => 500,
                        'minMessage' => 'Enter at least 1 character',
                        'maxMessage' => 'You entered {{ value }} but you can not use more than {{ limit }} characters',
                    ]),
                ],
            ])
            ->add('status', IntegerType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'The status cannot be blank']),
                    new Choice(['choices' => StatusConstants::ALL, 'message' => 'Invalid status {{ value }}']),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }
}
