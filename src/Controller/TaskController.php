<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\StatusConstants;
use App\Entity\Task;
use App\Form\TaskType;
use App\HttpKernel\ApiException;
use App\Repository\TaskRepository;
use DateTimeImmutable;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/v1/task", name="api_task")
 */
class TaskController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private TaskRepository $taskRepository;

    public function __construct(EntityManagerInterface $entityManager, TaskRepository $taskRepository)
    {
        $this->entityManager = $entityManager;
        $this->taskRepository = $taskRepository;
    }

    #[Route(name: 'api_task_create', methods: [Request::METHOD_POST])]
    /**
     * @OA\RequestBody(
     *  required=true,
     *  description="Creates a task. A *unique* value is required for **task**. Accepted values for **status** include
     *  1=Pending 2=In Progress 3=Done 4=Closed",
     *  @Model(type=Task::class, groups={"task_request_body"})
     * ),
     * @OA\Response(
     *  response=201,
     *  description="Returns the task",
     *  @OA\JsonContent(ref="#/components/schemas/ApiError")
     * ),
     * @OA\Response(
     *  response=400,
     *  description="Validation errors",
     *  @OA\JsonContent(ref="#/components/schemas/ApiError")
     * ),
     * @OA\Response(
     *  response=500,
     *  description="Service errors",
     *  @OA\JsonContent(ref="#/components/schemas/ApiError")
     * )
     */
    public function createAction(Request $request): JsonResponse
    {
        /** @var stdClass $content */
        $content = json_decode($request->getContent());

        $form = $this->createForm(TaskType::class);
        $nonObject = (array) $content;
        if (!isset($nonObject['status'])) {
            $nonObject['status'] = StatusConstants::PENDING;
        }
        $form->submit($nonObject);

        if (!$form->isValid()) {
            throw new ApiException(400, $this->validationErrorsToString($form));
        }

        $task = Task::fromFormFactory($form);

        try {
            $this->entityManager->persist($task);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException) {
            throw new ApiException(400, 'Task must be unique');
        }

        return $this->json($task, 201);
    }

    #[Route(name: 'api_task_read', methods: [Request::METHOD_GET])]
    /**
     * @OA\Response(
     *  response=200,
     *  description="Returns array of tasks",
     *  @OA\JsonContent(
     *    type="array",
     *    @OA\Items(ref=@Model(type=Task::class, groups={"task_response_body"}))
     *  )
     * ),
     * @OA\Response(
     *  response=500,
     *  description="Service errors",
     *  @OA\JsonContent(ref="#/components/schemas/ApiError")
     * )
     */
    public function readAction(): JsonResponse
    {
        $tasks = $this->taskRepository->findAll();
        return $this->json($tasks);
    }

    #[Route('/{id}', name: 'api_task_update', methods: [Request::METHOD_PUT])]
    /**
     * @OA\RequestBody(
     *  required=true,
     *  description="Updates a task. A *unique* value is required for **task**. Accepted values for **status** include
     *  1=Pending 2=In Progress 3=Done 4=Closed",
     *  @Model(type=Task::class, groups={"task_request_body"})
     * ),
     * @OA\Response(
     *  response=200,
     *  description="Returns the task",
     *  @Model(type=Task::class, groups={"task_response_body"})
     * ),
     * @OA\Response(
     *  response=400,
     *  description="Validation errors",
     *  @OA\JsonContent(ref="#/components/schemas/ApiError")
     * ),
     * @OA\Response(
     *  response=500,
     *  description="Service errors",
     *  @OA\JsonContent(ref="#/components/schemas/ApiError")
     * )
     */
    public function updateAction(Request $request, Task $task): JsonResponse
    {
        /** @var stdClass $content */
        $content = json_decode($request->getContent());

        $form = $this->createForm(TaskType::class);
        $nonObject = (array) $content;
        unset($nonObject['id']);
        $form->submit($nonObject);

        if (!$form->isValid()) {
            throw new ApiException(400, $this->validationErrorsToString($form));
        }

        if (0 === $task->compareTo(Task::fromFormFactory($form))) {
            throw new ApiException(400, 'There was no change to the Task');
        }

        $task = Task::fromFormFactory($form, $task);
        $task->setUpdatedAt(new DateTimeImmutable());

        $this->entityManager->persist($task);
        $this->entityManager->flush();

        return $this->json($task);
    }

    #[Route('/{id}', name: 'api_task_delete', methods: [Request::METHOD_DELETE])]
    /**
     * @OA\Response(
     *  response=204,
     *  description="Result of the delete operation"
     * ),
     * @OA\Response(
     *  response=500,
     *  description="Service errors",
     *  @OA\JsonContent(ref="#/components/schemas/ApiError")
     * )
     */
    public function deleteAction(Task $task): Response
    {
        $this->entityManager->remove($task);
        $this->entityManager->flush();

        return new Response(null, 204);
    }

    private function validationErrorsToString(FormInterface $form): string
    {
        $errors = [];
        /** @var FormError $error */
        foreach ($form->getErrors(true, true) as $error) {
            $propertyName = $error->getOrigin()?->getName() ?? 'entity';
            $errors[$propertyName] = $error->getMessage();
        }

        return implode(PHP_EOL, $errors);
    }
}
