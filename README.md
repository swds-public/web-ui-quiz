[[_TOC_]]

# Web UI Quiz

Thank you for your interest in joining the technology team at Second Wave Delivery Systems! 

## Jobs to be Done
- Create a new branch off of `main`
- Create a standalone Single-page App (SPA) using a UI framework of your choice (React, Vue, Angular preferred)
- Implement UI controls for basic CRUD actions using the provided API endpoint `/api/v1/task` for managing tasks
- Implement UI controls to list tasks in a table with sortable columns for each element of a task
- Implement UI controls to update the `Status` of a task 
- Include unit tests in your code
- Package SPA as a docker container

## API Documentation
You will find the API documentation provided as part of a Symfony application (API).
Once you have completed the steps to setup the API, the docs are available at http://localhost:8002/api/v1/doc

## Submission
In order to submit the test for review, please push your completed work upstream and open a new merge request.

# Application Instructions

## Setup
1. Install [Docker Engine](https://docs.docker.com/engine/install/)
2. ```git clone git@gitlab.com:swds-public/web-ui-quiz```
3. ```cd web-ui-quiz```
4. ```APP_HTTP_PORT=8002 docker compose up --build```
5. The API should now be available at http://localhost:8002/api/v1/doc
6. Note: Change the host exposed port as needed

## Package SPA 
Package your SPA as a docker container and include it in the existing `docker-compose.yml`. 

For example, you could include a new React based service with a `Dockerfile` located in the `./docker/ui` directory as follows:

`./docker/ui/Dockerfile`
```angular2html
FROM node:16-alpine3.15

WORKDIR /var/www/ui

COPY package.json /var/www/ui/
COPY yarn.lock /var/www/ui/
RUN yarn install

COPY . /var/www/ui

RUN yarn run build
CMD yarn start
```

`./docker-compose.yml`
```
version: '3.8'

services:
  ...
  ui:
    build: ./docker/ui
    restart: on-failure
    command: "npm start"
    depends_on:
      - app
    volumes:
      # this will mount the ui folder which contains the code to the docker container
      - ./docker/ui:/var/www/ui:delegated
      # this will mount the node_modules folder for faster performance
      - node_modules:/var/www/ui/node_modules
    ports:
      - "3000:3000"
   ...
```

## Supporting Material
- https://symfony.com/doc/5.4/index.html
- https://symfony.com/doc/5.4/controller.html
