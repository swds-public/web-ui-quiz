<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\StatusConstants;
use App\Entity\Task;
use App\Form\TaskType;
use DateTimeInterface;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormExtensionInterface;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\Validation;

class TaskTest extends TypeTestCase
{
    public function testFromFormFactoryCreateNew(): void
    {
        $data = [
            'task' => 'test task',
            'description' => 'test task description',
            'status' => StatusConstants::PENDING,
        ];
        $form = $this->factory->create(TaskType::class);
        $form->submit($data);
        $this->assertTrue($form->isValid());
        $task = Task::fromFormFactory($form);
        $this->assertEquals('test task', $task->getTask());
        $this->assertEquals('test task description', $task->getDescription());
        $this->assertEquals(StatusConstants::PENDING, $task->getStatus());
        $this->assertTrue($task->getCreatedAt()->getTimestamp() > time() - 10);
        $this->assertTrue($task->getUpdatedAt()->getTimestamp() > time() - 10);
    }

    public function testFromFormFactoryUpdateExisting(): void
    {
        $data = [
            'task' => 'test task',
            'description' => 'test task description',
            'status' => StatusConstants::PENDING,
        ];
        $form = $this->factory->create(TaskType::class);
        $form->submit($data);
        $this->assertTrue($form->isValid());
        $task = Task::fromFormFactory($form);
        $data['description'] = 'this is the same task';
        $form = $this->factory->create(TaskType::class);
        $form->submit($data);
        $sameTask = Task::fromFormFactory($form, $task);
        $this->assertSame($task, $sameTask);
        $this->assertEquals('test task', $sameTask->getTask());
        $this->assertEquals('this is the same task', $sameTask->getDescription());
    }

    public function testToArray(): void
    {
        $task = new Task(
            task: 'test task',
            description: 'test task description',
            status: StatusConstants::PENDING
        );
        $this->assertEquals(
            [
                'id' => null,
                'task' => 'test task',
                'description' => 'test task description',
                'status' => StatusConstants::PENDING,
                'createdAt' => $task->getCreatedAt()->format(DateTimeInterface::ATOM),
                'updatedAt' => $task->getUpdatedAt()->format(DateTimeInterface::ATOM),
            ],
            $task->toArray()
        );
    }

    public function testCompareToSame(): void
    {
        $task = new Task(
            task: 'test task',
            description: 'test task description',
            status: StatusConstants::PENDING
        );
        $sameTask = clone $task;
        $this->assertEquals(0, $task->compareTo($sameTask));
    }

    public function testCompareToDifferent(): void
    {
        $task = new Task(
            task: 'test task',
            description: 'test task description',
            status: StatusConstants::PENDING
        );
        $differentTask = clone $task;
        $differentTask->setDescription('new task description');
        $this->assertEquals(1, $task->compareTo($differentTask));
    }

    /**
     * @psalm-return list<FormExtensionInterface>
     */
    protected function getExtensions(): array
    {
        return [
            new ValidatorExtension(Validation::createValidator()),
        ];
    }
}
