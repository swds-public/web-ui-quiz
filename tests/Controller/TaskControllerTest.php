<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Entity\StatusConstants;
use App\Entity\Task;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\AbstractManagerRegistry;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskControllerTest extends WebTestCase
{
    public function testCreateAction(): void
    {
        $client = self::createClient();
        $client->request(
            method: 'POST',
            uri: '/api/v1/task',
            parameters: [],
            files: [],
            server: [],
            content: json_encode([
                'task' => $task = substr(uniqid(), 0, 10),
                'description' => $description = uniqid(),
            ]) ?: ''
        );
        $this->assertResponseStatusCodeSame(201);
        /** @var stdClass $response */
        $response = json_decode($client->getResponse()->getContent() ?: '[]');
        $this->assertNotEmpty($response->id);
        $this->assertEquals($task, $response->task);
        $this->assertEquals($description, $response->description);
        $this->assertEquals(StatusConstants::PENDING, $response->status);
        $this->assertNotEmpty($response->createdAt);
        $this->assertNotEmpty($response->updatedAt);
    }

    public function testReadAction(): void
    {
        $client = self::createClient();
        /** @var AbstractManagerRegistry $registry */
        $registry = static::getContainer()->get('doctrine');
        /** @var EntityManagerInterface $em */
        $em = $registry->getManager();
        /** @var TaskRepository $taskRepo */
        $taskRepo = $em->getRepository(Task::class);
        $em->persist(new Task(substr(uniqid(), 0, 10), 'test', StatusConstants::PENDING));
        $em->flush();
        $response = json_encode(call_user_func_array(fn (Task $t) => $t->toArray(), $taskRepo->findAll()));

        $client->request('GET', '/api/v1/task');
        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals($response, $client->getResponse()->getContent());
    }

    public function testUpdateAction(): void
    {
        $client = self::createClient();
        /** @var AbstractManagerRegistry $registry */
        $registry = static::getContainer()->get('doctrine');
        /** @var EntityManagerInterface $em */
        $em = $registry->getManager();
        $em->persist($task = new Task(substr(uniqid(), 0, 10), 'test', StatusConstants::PENDING));
        $em->flush();
        assert(null !== $id = $task->getId());

        $client->request(
            method: 'PUT',
            uri: "/api/v1/task/$id",
            parameters: [],
            files: [],
            server: [],
            content: json_encode([
                'task' => $newName = substr(uniqid(), 0, 10),
                'description' => $newDescription = uniqid(),
                'status' => StatusConstants::IN_PROGRESS,
            ]) ?: ''
        );
        $this->assertResponseStatusCodeSame(200);
        /** @var stdClass $response */
        $response = json_decode($client->getResponse()->getContent() ?: '[]');
        $this->assertEquals($newName, $response->task);
        $this->assertEquals($newDescription, $response->description);
        $this->assertEquals(StatusConstants::IN_PROGRESS, $response->status);
        $this->assertNotEmpty($response->createdAt);
        $this->assertNotEmpty($response->updatedAt);
    }

    public function testDeleteAction(): void
    {
        $client = self::createClient();
        /** @var AbstractManagerRegistry $registry */
        $registry = static::getContainer()->get('doctrine');
        /** @var EntityManagerInterface $em */
        $em = $registry->getManager();
        /** @var TaskRepository $taskRepo */
        $taskRepo = $em->getRepository(Task::class);
        $em->persist($task = new Task(substr(uniqid(), 0, 10), 'test', StatusConstants::PENDING));
        $em->flush();
        assert(null !== $id = $task->getId());

        $client->request(
            method: 'DELETE',
            uri: "/api/v1/task/$id",
        );
        $this->assertResponseStatusCodeSame(204);
        $this->assertNull($taskRepo->find($id));
    }
}
